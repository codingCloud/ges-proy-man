package com.codingcloud.gesproman.presenter;

import com.codingcloud.gesproman.model.LogActivitiesInteractor;
import com.codingcloud.gesproman.model.ProjectProgressResourcesInteractor;
import com.codingcloud.gesproman.mvp.LogActivitiesMVP;
import com.codingcloud.gesproman.mvp.ProjectProgressResourcesMVP;

public class ProjectProgressResourcesPresenter implements ProjectProgressResourcesMVP.Presenter{

    private ProjectProgressResourcesMVP.View view;
    private ProjectProgressResourcesMVP.Model model;

    public ProjectProgressResourcesPresenter(ProjectProgressResourcesMVP.View view) {
        this.view = view;
        this.model = new ProjectProgressResourcesInteractor(view.getActivity());
    }
    @Override
    public boolean saveClick() {
        ProjectProgressResourcesMVP.ProjectProgressResourcesInfo projectProgressResourcesInfo = view.getProjectProgressResourcesInfo();
            boolean error = false;

            if (projectProgressResourcesInfo.getActivity().trim().isEmpty()) {
                view.showGeneralError();
                error = true;
            }
            if (projectProgressResourcesInfo.getBudget().trim().isEmpty()) {
                view.showGeneralError();
                error = true;
            }
            if (projectProgressResourcesInfo.getRealBudget().trim().isEmpty()) {
                view.showGeneralError();
                error = true;
            }
                if (!error) {
                    view.onSaveClick();

                }
                return error;

            }
    }
