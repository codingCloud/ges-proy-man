package com.codingcloud.gesproman.presenter;

import com.codingcloud.gesproman.model.NewProjectInteractor;
import com.codingcloud.gesproman.mvp.LogActivitiesMVP;
import com.codingcloud.gesproman.mvp.NewProjectMVP;

public class NewProjectPresenter implements NewProjectMVP.Presenter {
    private NewProjectMVP.Model model;
    private NewProjectMVP.View view;

    public NewProjectPresenter(NewProjectMVP.View view) {
        this.model = new NewProjectInteractor();
        this.view = view;
    }

    public void nextClick() {
        NewProjectMVP.NewProjectInfo newProjectInfo = view.getNewProjectInfo();
        boolean error = false;

        if (newProjectInfo.getNameProject().trim().isEmpty()) {
            view.showGeneralError();
            error = true;
        }
        if (newProjectInfo.getBudGeneral().trim().isEmpty()) {
            view.showGeneralError();
            error = true;
        }
        /*
        if (newProjectInfo.getCant().trim().isEmpty()) {
            view.showGeneralError();
            error = true;
        }
        if (newProjectInfo.getSpTimeFrame().trim().isEmpty()) {
            view.showGeneralError();
            error = true;
        }
         */
        if (newProjectInfo.getNameAuditor().trim().isEmpty()) {
            view.showGeneralError();
            error = true;
        }
        if (newProjectInfo.getNameExecutor().trim().isEmpty()) {
            view.showGeneralError();
            error = true;
        }
        if (newProjectInfo.getNameOwner().trim().isEmpty()) {
            view.showGeneralError();
            error = true;
        }
        if (!error) {
            view.onNextClick();

        }
    }
}
