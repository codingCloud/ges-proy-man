package com.codingcloud.gesproman.presenter;

import com.codingcloud.gesproman.model.InfProjectInteractor;
import com.codingcloud.gesproman.mvp.InfProjectMVP;

public class InfProjectPresenter implements InfProjectMVP.Presenter {

    private InfProjectMVP.Model model;
    private InfProjectMVP.View view;

    public InfProjectPresenter(InfProjectMVP.View view) {
        this.model = new InfProjectInteractor();
        this.view = view;

    }
}
