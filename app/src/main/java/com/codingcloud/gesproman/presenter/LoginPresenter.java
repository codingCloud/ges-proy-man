package com.codingcloud.gesproman.presenter;

//import android.content.Context;
//import android.content.SharedPreferences;

import com.codingcloud.gesproman.model.LoginInteractor;
import com.codingcloud.gesproman.mvp.LoginMVP;

public class LoginPresenter implements LoginMVP.Presenter {

    //private final String AUTH_PREFERENCE = "authentication";
    //private final String LOGGED_KEY = "logged";

    private LoginMVP.View view;
    private LoginMVP.Model model;


    public LoginPresenter(LoginMVP.View view){
        this.view = view;
        //this.model = new LoginInteractor();
        this.model = new LoginInteractor(view.getActivity());
    }
    /*public void isLogged(){
        SharedPreferences preferences=view.getActivity()
                .getSharedPreferences(AUTH_PREFERENCE, Context.MODE_PRIVATE);
        boolean isLogged = preferences.getBoolean(LOGGED_KEY,false);
        if (isLogged){
            view.openNewActivity();
        }
    }*/
    @Override
    public void login() {
        boolean error = false;
        view.showEmailError("");
        view.showPasswordError("");
        LoginMVP.LoginInfo loginInfo=view.getLoginInfo();
        if (loginInfo.getEmail().trim().isEmpty()){
            view.showEmailError("Correo electronico es obligatorio");
            error = true;
        }else if(!isEmailValid(loginInfo.getEmail().trim())){
            view.showEmailError("Correo electronico no es valido");
            error = true;
        } 
        if (loginInfo.getPassword().trim().isEmpty()) {
            view.showPasswordError("Contraseña es obligatoria");
            error = true;
        }else if(!isPasswordValid(loginInfo.getPassword().trim())) {
            view.showPasswordError("Contraseña no es valida");
            error = true;
        }

        if(!error) {
            view.startWaiting();
            new Thread(()->{
                model.validateCredentials(
                        loginInfo.getEmail().trim(),
                        loginInfo.getPassword().trim(),
                        new LoginMVP.Model.ValidateCredentialsCallback(){
                            @Override
                            public void onSuccess(){
                               /* SharedPreferences preferences=view.getActivity()
                                        .getSharedPreferences(AUTH_PREFERENCE, Context.MODE_PRIVATE);
                                preferences.edit().putBoolean(LOGGED_KEY,true).apply();*/
                                view.getActivity().runOnUiThread(()->{
                                    view.stopWaiting();
                                    view.openNewActivity();
                                });
                            }
                            @Override
                            public void onFailure(String error){
                                view.getActivity().runOnUiThread(()->{
                                    view.stopWaiting();
                                    view.showGeneralMessage(error);
                                });

                            }
                        }
                );
            }).start();
        }
    }

    @Override
    public void isAuthenticated() {
        boolean isAuthenticated = model.isAuthenticated();
        if (isAuthenticated) {
            view.openNewActivity();
        }
    }

    private boolean isEmailValid(String email) {
        return email.contains("@") && email.contains(".");
    }
    private boolean isPasswordValid(String password) {
        boolean may=false;
        boolean min=false;
        for (int i=0;i<password.length();i++){
            if (Character.isUpperCase(password.charAt(i)))
                may=true;
            if (Character.isLowerCase(password.charAt(i)))
                min=true;
        }
        return password.length()>7 && may && min ;
    }

}
