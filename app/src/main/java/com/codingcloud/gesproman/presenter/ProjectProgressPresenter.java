package com.codingcloud.gesproman.presenter;

import com.codingcloud.gesproman.model.ProjectProgressInteractor;
import com.codingcloud.gesproman.mvp.ProjectProgressMVP;

public class ProjectProgressPresenter implements ProjectProgressMVP.Presenter {

    private ProjectProgressMVP.View view;
    private ProjectProgressMVP.Model model;

    public ProjectProgressPresenter(ProjectProgressMVP.View view) {
        this.view = view;
        this.model = new ProjectProgressInteractor();
    }

    @Override
    public void resourcesProgress() {
        view.openResourceProgress();
    }

    @Override
    public void timeProgress() {
        view.openTimeProgress();
    }

    @Override
    public void qualityProgress() {
        view.openQualityProgress();
    }
}
