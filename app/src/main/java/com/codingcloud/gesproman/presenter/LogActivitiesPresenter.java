package com.codingcloud.gesproman.presenter;

import android.os.Bundle;
import android.widget.TextView;

import com.codingcloud.gesproman.model.LogActivitiesInteractor;
import com.codingcloud.gesproman.mvp.LogActivitiesMVP;

import java.util.List;

public class LogActivitiesPresenter implements LogActivitiesMVP.Presenter {

    TextView numberActivity;
    private LogActivitiesMVP.View view;
    private LogActivitiesMVP.Model model;

    public LogActivitiesPresenter(LogActivitiesMVP.View view) {
        this.view = view;
        this.model = new LogActivitiesInteractor(view.getActivity());
    }

    @Override
    public void saveClick() { //Guardar actividad y limpiar campos
        LogActivitiesMVP.LogActivitiesInfo logActivitiesInfo = view.getLogActivitiesInfo();
        boolean error = false;

        if (logActivitiesInfo.getNameActivity().trim().isEmpty()) {
            view.showGeneralError();
            error = true;
        }
        if (logActivitiesInfo.getBudget().trim().isEmpty()) {
            view.showGeneralError();
            error = true;
        }
        if (logActivitiesInfo.getQualityParameters1().trim().isEmpty()) {
            view.showGeneralError();
            error = true;
        }
        if(!error) {
            view.onSaveClick();
            view.onClearClick();
        }
    }

    @Override
    public boolean addClick() { //Añadir una nueva actividad
        LogActivitiesMVP.LogActivitiesInfo logActivitiesInfo = view.getLogActivitiesInfo();
        boolean error = false;

        if (logActivitiesInfo.getNameActivity().trim().isEmpty() && logActivitiesInfo.getBudget().trim().isEmpty() && logActivitiesInfo.getQualityParameters1().trim().isEmpty()) {
            view.showGeneralError();
            error = true;
        }
        if (!error) {
            view.onAddClick();

        }
        return error;

    }

    @Override
    public void deleteClick() { //Eliminar la actividad
        LogActivitiesMVP.LogActivitiesInfo logActivitiesInfo = view.getLogActivitiesInfo();
        view.onDeleteClick();
    }

    @Override
    public boolean addParametersClick() {
        LogActivitiesMVP.LogActivitiesInfo logActivitiesInfo = view.getLogActivitiesInfo();
        boolean error = false;

        if (logActivitiesInfo.getQualityParameters1().trim().isEmpty()) {
            view.showParametersError();
            error = true;
        }
        if (!error) {
            view.onAddParametersClick();

        }
        return error;

    }

    @Override
    public void queryData() {
            new Thread(() -> {
                model.loadLogActivities(new LogActivitiesMVP.Model.LoadLogActivitiesCallback() {
                    @Override
                    public void showLogActivitiesInfo(List<LogActivitiesMVP.LogActivitiesParametersInfo> logActivitiesParametersInfo) {
                        view.getActivity().runOnUiThread(() -> {
                            view.showLogActivitiesInfo(logActivitiesParametersInfo);
                        });
                    }
                });
            }).start();
        }

    @Override
    public void onItemSelectedParameters(LogActivitiesMVP.LogActivitiesParametersInfo logActivitiesParametersInfo) {
        Bundle params = new Bundle();
        params.putString("name", logActivitiesParametersInfo.getParameters());
        view.openLocationActivity();
    }


    @Override
    public void onItemSelected(LogActivitiesMVP.LogActivitiesInfo logActivitiesInfo) {
        Bundle params = new Bundle();
        params.putString("name", logActivitiesInfo.getNameActivity());
        params.putString("budget", logActivitiesInfo.getBudget());
        params.putString("address", logActivitiesInfo.getQualityParameters1());
        view.openLocationActivity();
    }
}



