package com.codingcloud.gesproman.presenter;

import com.codingcloud.gesproman.model.ProjectProgressTimeInteractor;
import com.codingcloud.gesproman.mvp.ProjectProgressTimeMVP;


public class ProjectProgressTimePresenter implements ProjectProgressTimeMVP.Presenter {

    private ProjectProgressTimeMVP.View view;
    private ProjectProgressTimeMVP.Model model;

    public ProjectProgressTimePresenter(ProjectProgressTimeMVP.View view) {
        this.view = view;
        this.model = new ProjectProgressTimeInteractor();
    }

    @Override
    public void progressTime() {
        view.saveProgressTime();
    }
}
