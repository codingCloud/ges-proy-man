package com.codingcloud.gesproman.presenter;

import com.codingcloud.gesproman.model.StartingScreenInteractor;
import com.codingcloud.gesproman.mvp.StartingScreenMVP;

public class StartingScreenPresenter implements StartingScreenMVP.Presenter {
    private StartingScreenMVP.Model model;
    private StartingScreenMVP.View view;

    public StartingScreenPresenter(StartingScreenMVP.View view) {
        this.model= new StartingScreenInteractor();
        this.view = view;
    }
    public void signOff(){
        /*SharedPreferences preferences=view.getActivity()
                .getSharedPreferences("authentication", Context.MODE_PRIVATE);
        preferences.edit().putBoolean("logged",false).apply();
        view.getActivity().runOnUiThread(()->{*/
            view.onCloseClick();
      //  });



    }

}
