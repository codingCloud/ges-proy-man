package com.codingcloud.gesproman.presenter;

import android.os.Bundle;

import com.codingcloud.gesproman.model.ViewChangesInteractor;
import com.codingcloud.gesproman.mvp.RegisterMVP;
import com.codingcloud.gesproman.mvp.ViewChangesMVP;

import java.util.List;

public class ViewChangesPresenter implements ViewChangesMVP.Presenter {

    private ViewChangesMVP.View view;
    private ViewChangesMVP.Model model;

    //Falta constructor
    public ViewChangesPresenter(ViewChangesMVP.View view) {
        this.view = view;
        this.model = new ViewChangesInteractor(view.getActivity());
    }

    @Override
    public void queryData() { //TODO: Falta consultar datos del RecycleView
        new Thread(() -> {
            model.loadViewChanges(new ViewChangesMVP.Model.LoadViewChangesCallback() {
                @Override
                public void showViewChangesInfo(List<ViewChangesMVP.ViewChangesInfo> viewChangesInfo) {
                    view.getActivity().runOnUiThread(() -> {
                        view.showViewChangesInfo(viewChangesInfo);
                    });
                }
            });
        }).start();
    }

    @Override
    public void onItemSelected(ViewChangesMVP.ViewChangesInfo viewChangesInfo) {
        Bundle params = new Bundle();
        params.putString("name", viewChangesInfo.getInfRequest());
        params.putString("address", viewChangesInfo.getInfDescription());
        view.openLocationActivity();

    }

    //Validar habilitación de botones aprobar y desaprobar (si están limpios los TextView)
    /*
    @Override
    public void enableApproveClick() {
        ViewChangesMVP.ViewChangesInfo viewChangesInfo = view.getViewChangesInfo();
        boolean error = false;

        if (viewChangesInfo.getInfRequest().trim().isEmpty() && viewChangesInfo.getInfDescription().trim().isEmpty()) {
            view.disableApprove();
            error = true;
        } else {
            //if (!error) {
                view.enableApprove();
            //}
        }
    }
     */
    /*
    @Override
    public void enableDisapproveClick() {
        ViewChangesMVP.ViewChangesInfo viewChangesInfo = view.getViewChangesInfo();
        boolean error = false;

        if (viewChangesInfo.getInfRequest().trim().isEmpty() && viewChangesInfo.getInfDescription().trim().isEmpty()) {
            view.disableDisapprove();
            error = true;
        } else {
            view.enableDisapprove();
        }
    } */
}
