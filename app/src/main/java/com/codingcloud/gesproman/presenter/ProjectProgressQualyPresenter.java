package com.codingcloud.gesproman.presenter;

import com.codingcloud.gesproman.model.ProjectProgressQualyInteractor;
import com.codingcloud.gesproman.mvp.LogActivitiesMVP;
import com.codingcloud.gesproman.mvp.ProjectProgressQualyMVP;
import com.codingcloud.gesproman.view.ProjectProgressQualyActivity;

public class ProjectProgressQualyPresenter implements ProjectProgressQualyMVP.Presenter {

    //Atributos
    private ProjectProgressQualyMVP.View view;
    private ProjectProgressQualyMVP.Model model;
    
    //Constructor

    public ProjectProgressQualyPresenter(ProjectProgressQualyMVP.View view) {
        this.view = view;
        this.model = new ProjectProgressQualyInteractor(view.getActivity());
    }

    @Override
    public void saveClick() {
        ProjectProgressQualyMVP.ProjectProgressQualyInfo projectProgressQualyInfo = view.getProjectProgressQualyInfo();
        boolean error = false;

        if (projectProgressQualyInfo.getQuality1().trim().isEmpty()) {
            view.showGeneralError();
            error = true;
        }
        if (projectProgressQualyInfo.getQuality2().trim().isEmpty()) {
            view.showGeneralError();
            error = true;
        }
        if (!error){
            view.onSaveClick();
            view.onClearClick();
        }

    }
}
