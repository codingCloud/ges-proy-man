package com.codingcloud.gesproman.presenter;

import android.os.Bundle;

import com.codingcloud.gesproman.model.ListProjectInteractor;
import com.codingcloud.gesproman.mvp.ListProjectMVP;
import com.codingcloud.gesproman.mvp.ViewChangesMVP;

import java.util.List;


public class ListProjectPresenter implements ListProjectMVP.Presenter {

    private ListProjectMVP.View view;
    private ListProjectMVP.Model model;

    public ListProjectPresenter(ListProjectMVP.View view) {
        this.view = view;
        this.model = new ListProjectInteractor(view.getActivity());
    }

    @Override
    public void queryData() {
        new Thread(() -> {
            model.loadListProject(new ListProjectMVP.Model.LoadListProjectCallback() {
                @Override
                public void showListProjectInfo(List<ListProjectMVP.ListProjectInfo> listProjectInfo) {
                    view.getActivity().runOnUiThread(() -> {
                        view.showListProjectInfo(listProjectInfo);
                    });
                }
            });
        }).start();
    }

    @Override
    public void onItemSelected(ListProjectMVP.ListProjectInfo listProjectInfo) {
        Bundle params = new Bundle();
        params.putString("project", listProjectInfo.getProject());
        view.openLocationActivity();

    }
}
