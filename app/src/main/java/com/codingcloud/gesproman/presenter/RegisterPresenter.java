package com.codingcloud.gesproman.presenter;

import android.util.Log;

import com.codingcloud.gesproman.model.RegisterInteractor;
import com.codingcloud.gesproman.model.repository.UserRepository;
import com.codingcloud.gesproman.mvp.RegisterMVP;

public class RegisterPresenter implements RegisterMVP.Presenter {

    private RegisterMVP.View view;
    private RegisterMVP.Model model;

    public RegisterPresenter(RegisterMVP.View view) {
        this.view = view;
        this.model = new RegisterInteractor(view.getActivity());
    }

    /*public void validatePassword() { //Guardar actividad y limpiar campos
       // RegisterMVP.UserInfo activityInfo = (RegisterMVP.UserInfo) view. getActivity();
        RegisterMVP.UserInfo userInfo = (RegisterMVP.UserInfo) view. getUserInfo();
        boolean error = false;

        userInfo.getPassword();

        if (!isPasswordValid("true")){
            view.showPasswordError();
        } else{
        }
    }*/

    public void saveClick() { //Guardar actividad y limpiar

        boolean error = false;
        RegisterMVP.UserInfo userInfo = view.getUserInfo();
        System.out.println("aquí SaveClick");

        if (userInfo.getName().trim().isEmpty()) {
            view.showGeneralError();
            error = true;
        }
        if (userInfo.getIdentification().trim().isEmpty()) {
            view.showGeneralError();
            error = true;
        }
        if (userInfo.getProfession().trim().isEmpty()) {
            view.showGeneralError();
            error = true;
        }
        if (userInfo.getEmail().trim().isEmpty()) {
            view.showGeneralError();
            error = true;
        }
        if (userInfo.getPassword().trim().isEmpty()) {
            view.showGeneralError();
            error = true;
        }
        if (userInfo.getPhone().trim().isEmpty()) {
            view.showGeneralError();
            error = true;
        }
        if (userInfo.getType().trim().isEmpty()){
            view.showGeneralError();
            error=true;
        }
        if(!error) {
            System.out.println("aquí ");

            new Thread(()->{
                model.createUser(
                        userInfo,
                        new RegisterMVP.Model.CreateUserCallback() {

                            @Override
                            public void onSuccess() {

                                view.getActivity().runOnUiThread(()->{
                                    ///view.stopWaiting();
                                    Log.d(UserRepository.class.getSimpleName(),userInfo.toString());
                                    view.onCreateClick();

                                });
                            }

                            @Override
                            public void onFailure(String error) {
                                view.getActivity().runOnUiThread(()->{
                                    //view.stopWaiting();
                                    Log.d(UserRepository.class.getSimpleName(),userInfo.toString());
                                    view.showGeneralMessage(error);
                                });
                            }
                        }
                );
            }).start();
        }
    }

    @Override
    public boolean isPasswordValid(String password) {
        boolean may=false;
        boolean min=false;
        for (int i=0;i<password.length();i++){
            if (Character.isUpperCase(password.charAt(i)))
                may=true;
            if (Character.isLowerCase(password.charAt(i)))
                min=true;
        }
        return password.length()>7 && may && min ;
    }

}
