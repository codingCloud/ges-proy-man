package com.codingcloud.gesproman.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.codingcloud.gesproman.R;
import com.codingcloud.gesproman.mvp.LogActivitiesMVP;
import com.codingcloud.gesproman.mvp.ProjectProgressQualyMVP;
import com.codingcloud.gesproman.presenter.LogActivitiesPresenter;
import com.codingcloud.gesproman.presenter.ProjectProgressQualyPresenter;
import com.google.android.material.textfield.TextInputEditText;

public class ProjectProgressQualyActivity extends AppCompatActivity implements ProjectProgressQualyMVP.View {

    private ProjectProgressQualyMVP.Presenter presenter;

    ImageView ivLogo;
    Spinner list;
    TextInputEditText tfQualify1;
    TextInputEditText tfQualify2;
    TextInputEditText tfNote1;
    TextInputEditText tfNote2;
    TextView result;
    AppCompatButton btnEvidence;
    AppCompatButton btnSave;
    AppCompatButton btnClear;
    AppCompatButton btnClose;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_progress_qualy);

        presenter = new ProjectProgressQualyPresenter(this);

        initUI();

    }

    private void initUI() {

        ivLogo = findViewById(R.id.iv_logo);

        tfQualify1 = findViewById(R.id.tf_qualify1);
        tfQualify2 = findViewById(R.id.tf_qualify2);
        tfNote1 = findViewById(R.id.tf_note1);
        tfNote2 = findViewById(R.id.tf_note2);

        result= findViewById(R.id.result); //TODO: Falta resultado de calidad


        list = findViewById(R.id.spinner_activities);
        String[] option = {"Periodo tiempo de control", "xxxx 1", "xxxx 2", "xxxx 3", "xxxx 4"}; //Falta configurar dependiendo de información indicada en interfaz anterior
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, option);
        list.setAdapter(adapter);

        //btnEvidence = findViewById(R.id.btn_evidence);
        //btnEvidence.setOnClickListener((evt) -> onEvidenceClick());

        btnSave = findViewById(R.id.btn_save);
        btnSave.setOnClickListener((evt) -> presenter.saveClick());

        btnClear = findViewById(R.id.btn_clear);
//        btnClear.setOnClickListener((evt) -> onClearClick());

        btnClose = findViewById(R.id.btn_close);
//        btnClose.setOnClickListener((evt) -> onCloseClick());
    }

    private void onEvidenceClick() {//TODO: Falta
    }

    public void onClearClick() {
        tfQualify1.setText("");
        tfQualify2.setText("");
        tfNote1.setText("");
        tfNote2.setText("");
    }

    public void onCloseClick() {
        Intent intent = new Intent(this, StartingScreenActivity.class);
        startActivity(intent);
    }

    @Override
    public Activity getActivity() {
        return null;
    }

    public ProjectProgressQualyMVP.ProjectProgressQualyInfo getProjectProgressQualyInfo() {
        return new ProjectProgressQualyMVP.ProjectProgressQualyInfo (tfQualify1.getText().toString(), tfQualify2.getText().toString(), tfNote1.getText().toString(), tfNote2.getText().toString() );
    }

    @Override
    public void showGeneralError() {
        Toast.makeText(ProjectProgressQualyActivity.this, "Obligatorio completar todos los campos", Toast.LENGTH_SHORT).show();
    }

    public boolean onSaveClick() {
        Toast.makeText(ProjectProgressQualyActivity.this, "Datos guardados exitosamente", Toast.LENGTH_SHORT).show();
        return false;
    }

}