package com.codingcloud.gesproman.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.codingcloud.gesproman.R;
import com.codingcloud.gesproman.mvp.LogActivitiesMVP;
import com.codingcloud.gesproman.view.LogActivities;

import java.util.ArrayList;
import java.util.List;

public class LogActivitiesAdapter extends RecyclerView.Adapter<LogActivitiesAdapter.ViewHolder> {

    private static LogActivitiesMVP.View view;
    private List<LogActivitiesMVP.LogActivitiesInfo> data;
    private AdapterView.OnItemClickListener onItemClickListener;

    //Constructores
    public LogActivitiesAdapter() {
        this.data = new ArrayList<>();
    }

    public LogActivitiesAdapter(List<LogActivitiesMVP.LogActivitiesInfo> data) {
        this.data = data;
        notifyDataSetChanged(); //Actualizar datos
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    //Crear un nuevo objeto por cada uno de los elementos que se tenga en la pantalla
    @NonNull
    @Override
    public LogActivitiesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_log_activities, parent, false);

        return new LogActivitiesAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LogActivitiesAdapter.ViewHolder holder, int position) {
        LogActivitiesMVP.LogActivitiesInfo item = data.get(position);

        if (item != null) {
            holder.getTfId1().setText(item.getNameActivity());
            holder.getTfId2().setText(item.getBudget());
            holder.getTfQualityParameters1().setText(item.getQualityParameters1());
        }
    }

    //Asociar el método ViewHolder con los datos



    //Decir cuantos elementos tiene la vista
    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tfId1;
        private TextView tfId2;
        private TextView tfQualityParameters1;

        //Getters
        public TextView getTfId1() {
            return tfId1;
        }
        public TextView getTfId2() {
            return tfId2;
        }
        public TextView getTfQualityParameters1() {
            return tfQualityParameters1;
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            //(Función Clickeable)
            //itemView.setOnClickListener((evt) ->  view.viewRecicleView());

            tfId1 = itemView.findViewById(R.id.tf_id1);
            tfId2 = itemView.findViewById(R.id.tf_id2);
            tfQualityParameters1 = itemView.findViewById(R.id.tf_qualityParameters1);
        }
    }


    public interface OnItemClickListener {
        void onItemClick(LogActivitiesMVP.LogActivitiesInfo info);
    }

    public void setData(ArrayList listLogActivities) {
    }
}



