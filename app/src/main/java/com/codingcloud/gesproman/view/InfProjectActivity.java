package com.codingcloud.gesproman.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.codingcloud.gesproman.R;
import com.google.android.material.button.MaterialButton;

public class InfProjectActivity extends AppCompatActivity {

    MaterialButton btnDetails;
    MaterialButton btnClose;
    MaterialButton btnChanges;

    Spinner spTimeFrame;
    TextView vlBudget;
    TextView vlExpenses;
    TextView vlBalance;
    TextView tpInitial;
    TextView tpRun;
    TextView tpPending;
    TextView ptQuality;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inf_project);
        initUI();
    }

    private void initUI(){
        spTimeFrame = findViewById(R.id.sp_time_frame);
        /*String [] option ={"Selecciona periodo"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, option);
        spTimeFrame.setAdapter(adapter);*/
        vlBudget = findViewById(R.id.vl_budget);
        vlExpenses = findViewById(R.id.vl_expenses);
        vlBalance = findViewById(R.id.vl_balance);
        tpInitial = findViewById(R.id.tp_initial);
        tpRun = findViewById(R.id.tp_run);
        tpPending = findViewById(R.id.tp_pending);
        ptQuality = findViewById(R.id.pt_quality);

        btnChanges = findViewById(R.id.btn_changes);
        btnChanges.setOnClickListener((evt)-> onChangesClick());

        btnClose = findViewById(R.id.btn_close);
        btnClose.setOnClickListener((evt)-> onCloseClick());

        btnDetails = findViewById(R.id.btn_Details);
        btnDetails.setOnClickListener((evt)-> onDetailsClick());
    }
    private void onChangesClick(){
        Intent intent = new Intent(this, ViewChangesActivity.class);
        startActivity(intent);
    }
    private void onCloseClick(){
        Intent intent = new Intent(this, StartingScreenActivity.class);
        startActivity(intent);
    }
    private void onDetailsClick(){
        Intent intent = new Intent(this, ProjectProgressActivity.class);
        startActivity(intent);
    }
}