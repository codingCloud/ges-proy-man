package com.codingcloud.gesproman.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import com.codingcloud.gesproman.R;
import com.codingcloud.gesproman.mvp.LogActivitiesMVP;
import com.codingcloud.gesproman.mvp.ProjectProgressResourcesMVP;
import com.codingcloud.gesproman.presenter.LogActivitiesPresenter;
import com.codingcloud.gesproman.presenter.ProjectProgressResourcesPresenter;
import com.google.android.material.textfield.TextInputEditText;

public class ProjectProgressResourcesActivity extends AppCompatActivity implements ProjectProgressResourcesMVP.View {

    private ProjectProgressResourcesMVP.Presenter presenter;

    ImageView ivLogo;

    TextInputEditText tfActivity;
    TextInputEditText tfBudget;
    TextInputEditText tfRealBudget;

    AppCompatButton btnSave;
    AppCompatButton btnClear;
    AppCompatButton btnClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_progress_resources);

        presenter = new ProjectProgressResourcesPresenter(this);

        initUI();
    }
    private void initUI () {

        ivLogo = findViewById(R.id.iv_logo);

        tfActivity = findViewById(R.id.tf_activity);
        tfBudget = findViewById(R.id.tf_budget);
        tfRealBudget = findViewById(R.id.tf_real_budget);

        btnSave = findViewById(R.id.btn_save);
        btnSave.setOnClickListener((evt) -> presenter.saveClick());

        btnClear = findViewById(R.id.btn_clear);
        btnClear.setOnClickListener((evt) -> onCancelClick());

        btnClose = findViewById(R.id.btn_close);
        btnClose.setOnClickListener((evt) -> onCloseClick());
    }


    @Override
    public void onSaveClick() {
        Toast.makeText(this, "Actividad guardada exitosamente", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCancelClick() {
        tfActivity.setText("");
        tfBudget.setText("");
        tfRealBudget.setText("");
    }

    public void onCloseClick() {
        Intent intent = new Intent(this, ProjectProgressActivity.class);
        startActivity(intent);
    }

    @Override
    public Activity getActivity() {
        return this;
    }

    @Override
    public void showGeneralError() {
        Toast.makeText(ProjectProgressResourcesActivity.this, "Obligatorio completar todos los campos", Toast.LENGTH_SHORT).show();
    }

    @Override
    public ProjectProgressResourcesMVP.ProjectProgressResourcesInfo getProjectProgressResourcesInfo() {
        return new ProjectProgressResourcesMVP.ProjectProgressResourcesInfo( tfActivity.getText().toString(),tfBudget.getText().toString(), tfRealBudget.getText().toString());
    }

}

