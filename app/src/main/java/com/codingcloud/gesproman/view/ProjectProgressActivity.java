package com.codingcloud.gesproman.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.codingcloud.gesproman.view.ProjectProgressResourcesActivity;
import com.codingcloud.gesproman.R;
import com.codingcloud.gesproman.mvp.ProjectProgressMVP;
import com.codingcloud.gesproman.presenter.ProjectProgressPresenter;
import com.google.android.material.button.MaterialButton;

public class ProjectProgressActivity extends AppCompatActivity implements ProjectProgressMVP.View {
    TextView tvTitleProjectProgress;
    TextView tvPeriodControlTime;
    Spinner spPeriodTime;
    TextView tvPillarControl;
    MaterialButton btnResources;
    MaterialButton btnTime;
    MaterialButton btnQuality;
    MaterialButton btnBackProgress;
    MaterialButton btnModifyProgress;
    MaterialButton btnCloseProgress;
    ProjectProgressMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_progress);
        presenter = new ProjectProgressPresenter(this);
        initUI();
    }

    private void initUI() {
        tvTitleProjectProgress = findViewById(R.id.tv_TitleProjectProgress);
        tvPeriodControlTime = findViewById(R.id.tv_PeriodControlTime);
        spPeriodTime = findViewById(R.id.sp_PeriodTime);
        tvPillarControl = findViewById(R.id.tv_PillarControl);
        btnResources = findViewById(R.id.btn_Resources);
        btnTime = findViewById(R.id.btn_Time);
        btnQuality = findViewById(R.id.btn_Quality);
        btnBackProgress = findViewById(R.id.btn_BackProgress);
        //btnModifyProgress = findViewById(R.id.btn_ModifyProgress);
        btnCloseProgress = findViewById(R.id.btn_CloseProgress);
        Integer[] options = new Integer[6];
        for (int i = 0; i < 6; i++) {
            options[i] = i;
        }
        ArrayAdapter<Integer> adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, options);
        spPeriodTime.setAdapter(adapter);
        btnResources.setOnClickListener((evt) -> presenter.resourcesProgress());
        btnTime.setOnClickListener((evt) -> presenter.timeProgress());
        btnQuality.setOnClickListener((evt) -> presenter.qualityProgress());
        btnBackProgress.setOnClickListener((evt) -> onBackProgressClick());

        //btnModifyProgress.setOnClickListener((evt) -> onModifyProgressClick());

        btnCloseProgress.setOnClickListener((evt) -> onCloseClick());
    }

    private void onCloseClick() {
        Intent intent = new Intent(this, StartingScreenActivity.class);
        startActivity(intent);
    }

    private void onModifyProgressClick() {
//        Intent intent = new Intent(this, StartingScreenActivity.class);startActivity(intent);

    }

    private void onBackProgressClick() {
        Intent intent = new Intent(this, InfProjectActivity.class);
        startActivity(intent);
    }

    @Override
    public void openResourceProgress() {
        Intent intent = new Intent(this, ProjectProgressResourcesActivity.class);
        startActivity(intent);
    }

    @Override
    public void openTimeProgress() {
        Intent intent = new Intent(this, ProjectProgressTimeActivity.class);
        startActivity(intent);
    }

    @Override
    public void openQualityProgress() {
        Intent intent = new Intent(this, ProjectProgressQualyActivity.class);
        startActivity(intent);
    }
}