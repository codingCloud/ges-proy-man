package com.codingcloud.gesproman.view;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.codingcloud.gesproman.R;
import com.codingcloud.gesproman.mvp.LoginMVP;
import com.codingcloud.gesproman.presenter.LoginPresenter;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;


public class LoginActivity extends AppCompatActivity implements LoginMVP.View {

    private ImageView ivLogo;
    private TextInputEditText tfEmail;
    private TextInputEditText tfPassword;
    private TextInputLayout tlEmail;
    private TextInputLayout tlPassword;

    private MaterialButton btnLogin;
    private MaterialButton btnRegister;

    private LoginMVP.Presenter presenter;

    private LinearProgressIndicator piWaiting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        presenter=new LoginPresenter(this);
        presenter.isAuthenticated();
        initUI();

    }

    private void initUI(){

        piWaiting=findViewById(R.id.pi_Waiting);
        ivLogo = findViewById(R.id.iv_logo);
        tlEmail = findViewById(R.id.tl_email);
        tfEmail = findViewById(R.id.tf_email);
        tlPassword = findViewById(R.id.tl_password);
        tfPassword = findViewById(R.id.tf_password);

        btnLogin = findViewById(R.id.btn_ini);
        btnLogin.setOnClickListener((evt) -> presenter.login());

        btnRegister = findViewById(R.id.btn_reg);
        btnRegister.setOnClickListener((evt) -> openRegisterActivity());

    }

    @Override
    public Activity getActivity() {
        return this;
    }

    @Override
    public LoginMVP.LoginInfo getLoginInfo() {
        return new LoginMVP.LoginInfo(tfEmail.getText().toString(), tfPassword.getText().toString());
    }

    @Override
    public void showEmailError(String error) {
        tlEmail.setError(error);
    }

    @Override
    public void showPasswordError(String error) { tlPassword.setError(error); }

    @Override
    public void showGeneralMessage(String error) {
        Toast.makeText(LoginActivity.this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void openNewActivity() {
        Intent intent = new Intent(this, StartingScreenActivity.class);
        startActivity(intent);
    }

    @Override
    public void openRegisterActivity() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    @Override
    public void startWaiting() {
        piWaiting.setVisibility(View.VISIBLE);
        btnLogin.setEnabled(false);
        btnRegister.setEnabled(false);

    }

    @Override
    public void stopWaiting() {
        piWaiting.setVisibility(View.GONE);
        btnLogin.setEnabled(true);
        btnRegister.setEnabled(true);
    }
}