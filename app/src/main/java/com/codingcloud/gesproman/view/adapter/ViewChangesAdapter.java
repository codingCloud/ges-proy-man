package com.codingcloud.gesproman.view.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.codingcloud.gesproman.R;
import com.codingcloud.gesproman.mvp.ViewChangesMVP;
import com.codingcloud.gesproman.view.ViewChangesActivity;

import java.util.ArrayList;
import java.util.List;

public class ViewChangesAdapter extends RecyclerView.Adapter<ViewChangesAdapter.ViewHolder> {

    private static ViewChangesMVP.View view;
    private List<ViewChangesMVP.ViewChangesInfo> data;
    private AdapterView.OnItemClickListener onItemClickListener;

    //Constructores
    public ViewChangesAdapter() {
        this.data = new ArrayList<>();
    }

    public void setData(List<ViewChangesMVP.ViewChangesInfo> data) {
        this.data = data;
        notifyDataSetChanged(); //Actualizar datos
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    //Crear un nuevo objeto por cada uno de los elementos que se tenga en la pantalla
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_view_changes, parent, false);

        return new ViewHolder(view);
    }
    //Asociar el método ViewHolder con los datos
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ViewChangesMVP.ViewChangesInfo item = data.get(position);

        if (item != null) {
            //TODO: holder.getTvClient().setImageIcon();
            holder.getInfRequest().setText(item.getInfRequest());
            holder.getInfDescription().setText(item.getInfDescription());
        }
    }
    //Decir cuantos elementos tiene la vista
    @Override
    public int getItemCount() {
        return data.size();
    }
    //Convertir objetos de una lista en elementos visibles dentro de la pantalla
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView infRequest;
        private TextView infDescription;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            //(Función Clickeable)
            //itemView.setOnClickListener((evt) -> view.approveDisapprove());

            infRequest = itemView.findViewById(R.id.info_request);
            infDescription = itemView.findViewById(R.id.info_description);
        }

        //Get
        public TextView getInfRequest() {
            return infRequest;
        }
        public TextView getInfDescription() {
            return infDescription;
        }

        public interface OnItemClickListener {
            void onItemClick(ViewChangesMVP.ViewChangesInfo info);
        }

    }


}
