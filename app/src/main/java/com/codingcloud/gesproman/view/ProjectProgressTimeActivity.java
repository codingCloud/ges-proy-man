package com.codingcloud.gesproman.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;

import com.codingcloud.gesproman.R;
import com.codingcloud.gesproman.mvp.ProjectProgressTimeMVP;
import com.codingcloud.gesproman.presenter.ProjectProgressTimePresenter;
import com.google.android.material.button.MaterialButton;

public class ProjectProgressTimeActivity extends AppCompatActivity implements ProjectProgressTimeMVP.View {
    TextView tvTitleProjectProgress;
    TextView tvSubTitlePillarTime;
    TextView tvActivityName1;
    CheckBox cbxActivityYes1;
    CheckBox cbxActivityNo1;
    Spinner spPercent;
    MaterialButton btnSaveProgressTime;
    MaterialButton btnCancelProgressTime;
    MaterialButton btnCloseProgressTime;
    ProjectProgressTimeMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_progress_time);
        presenter = new ProjectProgressTimePresenter(this);
        initUI();
    }

    private void initUI() {
        tvTitleProjectProgress = findViewById(R.id.tv_TitleProjectProgress);
        tvSubTitlePillarTime = findViewById(R.id.tv_SubTitlePillarTime);
        tvActivityName1 = findViewById(R.id.tv_ActivityName1);
        cbxActivityYes1 = findViewById(R.id.cbx_ActivityYes1);
        cbxActivityNo1 = findViewById(R.id.cbx_ActivityNo1);
        spPercent = findViewById(R.id.sp_Percent);
        btnSaveProgressTime = findViewById(R.id.btn_SaveProgressTime);
        btnCancelProgressTime = findViewById(R.id.btn_CancelProgressTime);
        btnCloseProgressTime = findViewById(R.id.btn_CloseProgressTime);
        Integer[] options = new Integer[101];
        for (int i = 0; i < 101; i++) {
            options[i] = i;
        }
        ArrayAdapter<Integer> adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, options);
        spPercent.setAdapter(adapter);

        btnSaveProgressTime.setOnClickListener((evt) -> presenter.progressTime());

        btnCancelProgressTime.setOnClickListener((evt) -> onCancelClick());

        btnCloseProgressTime.setOnClickListener((evt) -> onCloseClick());
    }

    private void onCloseClick() {
        Intent intent = new Intent(this, ProjectProgressActivity.class);
        startActivity(intent);
    }

    private void onCancelClick() {
        Intent intent = new Intent(this, StartingScreenActivity.class);
        startActivity(intent);
    }

    @Override
    public void saveProgressTime() {

    }
}