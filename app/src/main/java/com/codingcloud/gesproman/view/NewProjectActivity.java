package com.codingcloud.gesproman.view;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.codingcloud.gesproman.R;
import com.codingcloud.gesproman.mvp.LogActivitiesMVP;
import com.codingcloud.gesproman.mvp.NewProjectMVP;
import com.codingcloud.gesproman.presenter.LogActivitiesPresenter;
import com.codingcloud.gesproman.presenter.NewProjectPresenter;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

public class NewProjectActivity extends AppCompatActivity implements NewProjectMVP.View {

    private NewProjectMVP.Presenter presenter;

    MaterialButton btnNext;
    MaterialButton btnClose;
    MaterialButton btnClear;

    TextInputEditText tfNameProject;
    TextInputEditText tfBudGeneral;
    TextInputEditText tfCant;
    Spinner spTimeFrame;
    TextInputEditText tfNameOwner;
    TextInputEditText tfNameExecutor;
    TextInputEditText tfNameAuditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_project);

        presenter = new NewProjectPresenter(this);

        initUI();
    }
    private void initUI(){
        tfNameProject = findViewById(R.id.tf_name_project);
        tfBudGeneral = findViewById(R.id.tf_bud_general);
        tfCant = findViewById(R.id.tf_cant);
        spTimeFrame = findViewById(R.id.sp_time_frame);
        tfNameOwner = findViewById(R.id.tf_name_owner);
        tfNameExecutor = findViewById(R.id.tf_name_executor);
        tfNameAuditor = findViewById(R.id.tf_name_auditor);

        btnNext = findViewById(R.id.btn_next);
        btnNext.setOnClickListener((evt)-> presenter.nextClick());

        btnClose = findViewById(R.id.btn_close);
        btnClose.setOnClickListener((evt)-> onCloseClick());

        btnClear = findViewById(R.id.btn_clear);
        btnClear.setOnClickListener((evt)-> onClearClick());
    }
    public void onNextClick(){
        Intent intent = new Intent(this,LogActivities.class);
        startActivity(intent);
    }
    public void onCloseClick(){
        Intent intent = new Intent(this, StartingScreenActivity.class);
        startActivity(intent);
    }
    public void onClearClick(){
        tfNameProject.setText("");
        tfBudGeneral.setText("");
        tfCant.setText("");
        spTimeFrame.setSelection(0);
        tfNameOwner.setText("");
        tfNameExecutor.setText("");
        tfNameAuditor.setText("");
    }

    public Activity getActivity() {
        return this;
    }

    @Override
    public NewProjectMVP.NewProjectInfo getNewProjectInfo() { //Faltan los componentes tfCant y spTimeFrame
            return new NewProjectMVP.NewProjectInfo( tfNameProject.getText().toString(),tfBudGeneral.getText().toString(), tfNameOwner.getText().toString(),tfNameExecutor.getText().toString(),tfNameAuditor.getText().toString());
    }

    @Override
    public void showGeneralError() {
        Toast.makeText(NewProjectActivity.this, "Obligatorio completar todos los campos", Toast.LENGTH_SHORT).show();
    }
}