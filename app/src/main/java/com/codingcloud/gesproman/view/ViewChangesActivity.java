package com.codingcloud.gesproman.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.codingcloud.gesproman.R;
import com.codingcloud.gesproman.mvp.ViewChangesMVP;
import com.codingcloud.gesproman.presenter.ViewChangesPresenter;
import com.codingcloud.gesproman.view.adapter.ViewChangesAdapter;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;

public class ViewChangesActivity extends AppCompatActivity implements ViewChangesMVP.View {

    private ViewChangesMVP.Presenter presenter;
    private ViewChangesAdapter viewChangesAdapter;


    ImageView ivLogo;

    TextView infRequest;
    TextView infDescription;

    private NavigationView approveDisapprove;

    private RecyclerView rvViewChanges;

    AppCompatButton btnApprove;
    AppCompatButton btnDisapprove;
    AppCompatButton btnReturn;
    AppCompatButton btnClose;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_changes);

        presenter = new ViewChangesPresenter(this);
        //presenter.enableDisapproveClick();
        //presenter.enableApproveClick();

        initUI();

        presenter.queryData(); //Método de carga de datos
    }

    private void initUI () {

        //approveDisapprove = findViewById(R.id.approveDisapprove);
        //approveDisapprove.setNavigationItemSelectedListener(menuItem -> navigationItemSelected(menuItem));

        infRequest = findViewById(R.id.info_request);
        infDescription = findViewById(R.id.info_description);
        ArrayList listViewChanges = new ArrayList<ViewChangesMVP.ViewChangesInfo>();
        listViewChanges.add(new ViewChangesMVP.ViewChangesInfo("",""));

        ivLogo = findViewById(R.id.iv_logo);

        rvViewChanges = findViewById(R.id.rv_view_changes);
        rvViewChanges.setLayoutManager(new LinearLayoutManager(ViewChangesActivity.this));
        viewChangesAdapter = new ViewChangesAdapter();
        viewChangesAdapter.setData(listViewChanges);
        rvViewChanges.setAdapter(viewChangesAdapter);


        //btnApprove = findViewById(R.id.btn_approve);
        //btnApprove.setOnClickListener((evt) -> onApproveClick());

        //btnDisapprove = findViewById(R.id.btn_disapprove);
        //btnDisapprove.setOnClickListener((evt) -> onDisapproveClick());

        btnReturn = findViewById(R.id.btn_return);
        btnReturn.setOnClickListener((evt) -> onReturnClick());

        btnClose = findViewById(R.id.btn_close);
        btnClose.setOnClickListener((evt) -> onCloseClick());

    }

    //TODO: Falta colocar el siguiente cambio o indicar que no hay más cambios en botones de abrobar y desaprobar

    @Override
    public Activity getActivity() { return this; }

    @Override
    public void showViewChangesInfo(List<ViewChangesMVP.ViewChangesInfo> viewChangesInfo) {
        viewChangesAdapter.setData(viewChangesInfo);
    }
    // public Object getActivity() { return null; }

    @Override
    public ViewChangesMVP.ViewChangesInfo getViewChangesInfo() {
        //try {
            return new ViewChangesMVP.ViewChangesInfo(infRequest.getText().toString(), infDescription.getText().toString());
        //} catch (NullPointerException e){System.out.print("NullPointerException");}
        //return null;
    }

    public void approveDisapprove() { //Conectar a interfaz de botones aprobar y desaprobar
        Intent intent = new Intent(this, ViewChangesActivity.class);
        startActivity(intent);
    }

    public void onApproveClick(){
        infRequest.setText("");
        infDescription.setText("");
        Toast.makeText(this,"Cambio aprobado",Toast.LENGTH_SHORT).show();
    }

    public void onDisapproveClick(){
        infRequest.setText("");
        infDescription.setText("");
        Toast.makeText(this,"Cambio desaprobado",Toast.LENGTH_SHORT).show();
    }

    public void onReturnClick(){
       Intent intent = new Intent(this, InfProjectActivity.class);
        startActivity(intent);
    }

    public void onCloseClick(){
        Intent intent = new Intent (this, StartingScreenActivity.class);
        startActivity(intent);
    }

    /*
    @Override
    public void disableApprove() { //TODO: Revisar métodos de habilitación de botones
        btnApprove.setEnabled(false);
        //btnApprove.setAllowClickWhenDisabled(true);
    }
    @Override
    public void enableApprove() {
        btnApprove.setEnabled(true);
    }

    @Override
    public void disableDisapprove() {
        btnDisapprove.setEnabled(false);
        //btnApprove.setAllowClickWhenDisabled(true);
    }

    @Override
    public void enableDisapprove() {
        btnDisapprove.setEnabled(true);
    }
     */
    @Override
    public void showNoChanges() {
        Toast.makeText(ViewChangesActivity.this,"No hay más cambios pendientes", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void openLocationActivity() {

    }


}


