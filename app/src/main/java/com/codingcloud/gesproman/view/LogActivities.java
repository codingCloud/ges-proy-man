package com.codingcloud.gesproman.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.codingcloud.gesproman.R;
import com.codingcloud.gesproman.mvp.LogActivitiesMVP;
import com.codingcloud.gesproman.mvp.ViewChangesMVP;
import com.codingcloud.gesproman.presenter.LogActivitiesPresenter;
import com.codingcloud.gesproman.view.adapter.LogActivitiesAdapter;
import com.codingcloud.gesproman.view.adapter.ViewChangesAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;

public class LogActivities extends AppCompatActivity implements LogActivitiesMVP.View {

    private LogActivitiesMVP.Presenter presenter;
    private LogActivitiesAdapter logActivitiesAdapter;

    ImageView ivLogo;
    Spinner list;
    TextView numberActivity;
    int cont = 1;
    TextInputEditText tfId1;
    TextInputEditText tfId2;
    TextInputEditText tfQualityParameters1;
    TextInputEditText tfQualityParameters2;
    TextView parameters;

    private RecyclerView rvLogActivities;

    FloatingActionButton floatingActionButton1; //Más actividades
    FloatingActionButton floatingActionButton2; //Quitar actividad
    FloatingActionButton floatingActionButton3; //Más parámetros
    FloatingActionButton floatingActionButton4; //Quitar parámetro
    List<String> listLogParameters;
    AppCompatButton btnSave;
    AppCompatButton btnClear;
    AppCompatButton btnClose;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_activities);

        presenter = new LogActivitiesPresenter(this);

        initUI();

        presenter.queryData(); //Método de carga de datos
    }

    private void initUI() {

        ivLogo = findViewById(R.id.iv_logo);

            tfId1 = findViewById(R.id.tf_id1);
            tfId2 = findViewById(R.id.tf_id2);
            tfQualityParameters1 = findViewById(R.id.tf_qualityParameters1);
        ArrayList listLogActivities = new ArrayList<LogActivitiesMVP.LogActivitiesParametersInfo>();
        listLogActivities.add(new LogActivitiesMVP.LogActivitiesParametersInfo(""));


        rvLogActivities = findViewById(R.id.rv_log_activities);
        rvLogActivities.setLayoutManager(new LinearLayoutManager(LogActivities.this));
        logActivitiesAdapter = new LogActivitiesAdapter();
        logActivitiesAdapter.setData(listLogActivities);
        rvLogActivities.setAdapter(logActivitiesAdapter);


        list = findViewById(R.id.sp_type);
        String[] option = {"Periodo tiempo de control", "xxxx 1", "xxxx 2", "xxxx 3", "xxxx 4"}; //Falta configurar dependiendo de información indicada en interfaz anterior (tiempo y periodo de tiempo del proyecto)
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, option);
        list.setAdapter(adapter);

        numberActivity = findViewById(R.id.number_activity); //Falta programar lo del No. de la actividad


        floatingActionButton1 = findViewById(R.id.floatingActionButton1);
        floatingActionButton1.setOnClickListener((evt) -> presenter.addClick()); //Colocar método en presenter

        //floatingActionButton2 = findViewById(R.id.floatingActionButton2);
        //floatingActionButton2.setOnClickListener((evt) -> presenter.deleteClick()); //Colocar método en presenter

        floatingActionButton3 = findViewById(R.id.floatingActionButton3);
        floatingActionButton3.setOnClickListener((evt) -> presenter.addParametersClick());

        floatingActionButton4 = findViewById(R.id.floatingActionButton4);
        floatingActionButton4.setOnClickListener((evt) -> onDeleteParameterClick());

        btnSave = findViewById(R.id.btn_save);
        btnSave.setOnClickListener((evt) -> presenter.saveClick());

        btnClear = findViewById(R.id.btn_clear);
        btnClear.setOnClickListener((evt) -> onClearClick());

        btnClose = findViewById(R.id.btn_close);
        btnClose.setOnClickListener((evt) -> onCloseClick());
    }

    public void onAddClick() { //Colocar en presenter
        cont = cont + 1;
        numberActivity.setText(String.valueOf(cont));
        tfId1.setText("");
        tfId2.setText("");
        tfQualityParameters1.setText("");
    }//Falta que guarde

    public void onDeleteClick() {
        if (cont != 1) {
            cont = cont - 1;
            numberActivity.setText(String.valueOf(cont));
        } else {
            showActivityError();
        }
    } //Falta que coloque la información de la actividad anterior

    public void onAddParametersClick(){
        onClearClick();
        Toast.makeText(this, "Parametro añadido exitosamente", Toast.LENGTH_SHORT).show();

        /*listLogParameters.add(tfQualityParameters1.getText().toString());
        if (listLogParameters.size()==0){
            //tfQualityParameters2.setText(tfQualityParameters1.getText().toString());} else {
            System.out.print("tamaño lista "+listLogParameters.size());
            for (int i = 0; i < listLogParameters.size(); i++) {
                tfQualityParameters2.setText(i);
            }
        } */
    } //Agregar un nuevo parámetro

    private void onDeleteParameterClick() {
        onClearClick();
    } //Eliminar según parámetro indicado

    public boolean onSaveClick() {
        Toast.makeText(this, "Actividad guardada exitosamente", Toast.LENGTH_SHORT).show();
        return false;
    }

        public void onClearClick(){
            tfId1.setText("");
            tfId2.setText("");
            tfQualityParameters1.setText("");
        }

    @Override
    public void openLocationActivity() {

    }

    private void onCloseClick() {
        Intent intent = new Intent(this, StartingScreenActivity.class);
        startActivity(intent);
    }

    @Override
    public Activity getActivity() {
        return this;
    }

    @Override
    public LogActivitiesMVP.LogActivitiesInfo getLogActivitiesInfo() {
        return new LogActivitiesMVP.LogActivitiesInfo( tfId1.getText().toString(),tfId2.getText().toString(),tfQualityParameters1.getText().toString());
    }

    @Override
    public LogActivitiesMVP.LogActivitiesParametersInfo getLogActivitiesParametersInfo() {
        return new LogActivitiesMVP.LogActivitiesParametersInfo( parameters.getText().toString());
    }

    @Override
    public void showLogActivitiesInfo(List<LogActivitiesMVP.LogActivitiesParametersInfo> logActivitiesInfo) {

    }

    public void showGeneralError() {
        Toast.makeText(LogActivities.this, "Obligatorio completar todos los campos", Toast.LENGTH_SHORT).show();
    }

    public void showParametersError() {
        Toast.makeText(LogActivities.this, "Obligatorio completar el campo", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showActivityError() {
        Toast.makeText(LogActivities.this, "Debe haber almenos una actividad a implementar", Toast.LENGTH_SHORT).show();
    }

}