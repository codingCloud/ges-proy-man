package com.codingcloud.gesproman.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.codingcloud.gesproman.R;
import com.codingcloud.gesproman.mvp.ListProjectMVP;

import java.util.ArrayList;
import java.util.List;

public class ListProjectAdapter extends RecyclerView.Adapter<ListProjectAdapter.ViewHolder> {

    //private static ListProjectMVP.View view;
    private List<ListProjectMVP.ListProjectInfo> data;
    private OnItemClickListener onItemClickListener;

    //Constructores
    public ListProjectAdapter() {
        this.data = new ArrayList<>();
    }

    public void setData(List<ListProjectMVP.ListProjectInfo> data) {
        this.data = data;
        notifyDataSetChanged(); //Actualizar datos
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    //Crear un nuevo objeto por cada uno de los elementos que se tenga en la pantalla
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_project, parent, false);

        return new ViewHolder(view);
    }

    //Asociar el método ViewHolder con los datos
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ListProjectMVP.ListProjectInfo item = data.get(position);

        if (onItemClickListener != null) {
            //TODO: holder.getTvClient().setImageIcon();
            holder.itemView.setOnClickListener(v -> onItemClickListener.onItemClick(item));
        }
        holder.getTvProject().setText(item.getProject());
    }

    //Decir cuantos elementos tiene la vista
    @Override
    public int getItemCount() {
        return data.size();
    }

    //Convertir objetos de una lista en elementos visibles dentro de la pantalla
    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvProject;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvProject = itemView.findViewById(R.id.project);
        }

        //Get
        public TextView getTvProject() {
            return tvProject;
        }

    }

    public interface OnItemClickListener {
        void onItemClick(ListProjectMVP.ListProjectInfo info);

    }
}
