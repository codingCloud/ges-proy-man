package com.codingcloud.gesproman.view;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.codingcloud.gesproman.R;
import com.codingcloud.gesproman.mvp.StartingScreenMVP;
import com.codingcloud.gesproman.presenter.StartingScreenPresenter;
import com.google.android.material.button.MaterialButton;

public class StartingScreenActivity extends AppCompatActivity implements StartingScreenMVP.View {

    MaterialButton btnNew;
    MaterialButton btnSearch;
    MaterialButton btnClose;
    MaterialButton btnView;
    TextView nbUser;
    private StartingScreenMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starting_screen);
        presenter = new StartingScreenPresenter(this);

        initUI();
    }

    private void initUI(){
        nbUser = findViewById(R.id.nb_user);
        nbUser.setText("Roger Carrascal");
        btnNew = findViewById(R.id.btn_new);
        btnNew.setOnClickListener((evt) -> onNewClick());

        btnClose = findViewById(R.id.btn_close);
        btnClose.setOnClickListener((evt)-> presenter.signOff());

        btnSearch = findViewById(R.id.btn_search);
        btnSearch.setOnClickListener((evt) -> onSearchClick());

        btnView = findViewById(R.id.btn_view);
        btnView.setOnClickListener((evt)-> onViewClick());
    }

    private void onNewClick(){
        Intent intent = new Intent(this, NewProjectActivity.class);
        startActivity(intent);
    }

    @Override
    public Activity getActivity() {
        return this;
    }

    public void onCloseClick(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
    private void onSearchClick(){
        Intent intent = new Intent(this, ListProjectActivity.class);
        startActivity(intent);
    }
    private void onViewClick(){
        Intent intent = new Intent(this, InfProjectActivity.class);
        startActivity(intent);
    }

}