package com.codingcloud.gesproman.view;

import androidx.appcompat.app.AppCompatActivity;

//import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
//import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.codingcloud.gesproman.R;
import com.codingcloud.gesproman.mvp.RegisterMVP;
import com.codingcloud.gesproman.presenter.RegisterPresenter;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

public class RegisterActivity extends AppCompatActivity implements RegisterMVP.View{

    private Spinner spType;
    private MaterialButton btnCreate;
    private MaterialButton btnClose;
    private MaterialButton btnClear;

    private TextInputEditText tfName;
    private TextInputEditText tfIdentification;
    private TextInputEditText tfProfession;
    private TextInputEditText tfEmail;

    private TextInputEditText tfPhone;
    private TextInputEditText tfPassword;

    private RegisterMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        presenter = new RegisterPresenter(this);
        initUI();

    }

    private void initUI(){

        tfName = findViewById(R.id.tf_name);
        tfIdentification = findViewById(R.id.tf_id);
        tfProfession = findViewById(R.id.tf_profession);
        tfEmail = findViewById(R.id.tf_email);
        tfPhone  = findViewById(R.id.tf_phone);
        tfPassword = findViewById(R.id.tf_password);
        spType = findViewById(R.id.sp_type);

        btnCreate = findViewById(R.id.btn_create);
        btnCreate.setOnClickListener((evt)-> presenter.saveClick());

        btnClose = findViewById(R.id.btn_close);
        btnClose.setOnClickListener((evt)-> onCloseClick());

        btnClear = findViewById(R.id.btn_clear);
        btnClear.setOnClickListener((evt)-> onClearClick());

    }

    @Override
    public Activity getActivity() {
        return this;
    }

    @Override
    public RegisterMVP.UserInfo getUserInfo() {
        return new RegisterMVP.UserInfo(tfName.getText().toString(), tfIdentification.getText().toString(), tfProfession.getText().toString(), tfEmail.getText().toString(), tfPassword.getText().toString(), tfPhone.getText().toString(), spType.getSelectedItem().toString());
    }

    public void onCreateClick(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);

    }

    @Override
    public void showGeneralMessage(String error) {
        Toast.makeText(RegisterActivity.this, "Correo ya esta Registrado", Toast.LENGTH_SHORT).show();
    }

    private void onCloseClick(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
    public void onClearClick(){
        tfName.setText("");
        tfIdentification.setText("");
        tfProfession.setText("");
        tfEmail.setText("");
        tfPhone.setText("");
        tfPassword.setText("");
        spType.setSelection(0);
    }

    public void showPasswordError() {
        Toast.makeText(RegisterActivity.this, "Contraseña no es valida (debe tener más de 7 caracteres)", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showGeneralError() {
        Toast.makeText(RegisterActivity.this, "Obligatorio llenar todos los campos", Toast.LENGTH_SHORT).show();
    }
    @Override
    public void validatePassword() {

    }
   
}