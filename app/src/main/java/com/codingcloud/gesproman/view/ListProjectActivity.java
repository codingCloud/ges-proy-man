package com.codingcloud.gesproman.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.codingcloud.gesproman.R;
import com.codingcloud.gesproman.mvp.ListProjectMVP;
import com.codingcloud.gesproman.mvp.ViewChangesMVP;
import com.codingcloud.gesproman.presenter.ListProjectPresenter;
import com.codingcloud.gesproman.presenter.ViewChangesPresenter;
import com.codingcloud.gesproman.view.adapter.ListProjectAdapter;
import com.codingcloud.gesproman.view.adapter.ViewChangesAdapter;

import java.util.ArrayList;
import java.util.List;

public class ListProjectActivity extends AppCompatActivity implements ListProjectMVP.View {

    private ListProjectMVP.Presenter presenter;
    private ListProjectAdapter listProjectAdapter;

    ImageView ivLogo;

    TextView project;
    private RecyclerView rvListProject;

    AppCompatButton btnReturn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_project);

        presenter = new ListProjectPresenter(this);

        initUI();

        presenter.queryData(); //Método de carga de datos
    }

    private void initUI () {

        project= findViewById(R.id.result);

        ArrayList listListProject= new ArrayList<ListProjectMVP.ListProjectInfo>();
        listListProject.add(new ListProjectMVP.ListProjectInfo(""));


        ivLogo = findViewById(R.id.iv_logo);

        rvListProject = findViewById(R.id.rv_list_project);
        rvListProject.setLayoutManager(new LinearLayoutManager(ListProjectActivity.this));

        listProjectAdapter = new ListProjectAdapter();
        listProjectAdapter.setData(listListProject);

        rvListProject.setAdapter(listProjectAdapter);

        btnReturn = findViewById(R.id.btn_return);
        btnReturn.setOnClickListener((evt) -> onReturnClick());
}


    @Override
    public Activity getActivity() { return this; }

    @Override
    public void showListProjectInfo(List<ListProjectMVP.ListProjectInfo> listProjectInfo) {
        listProjectAdapter.setData(listProjectInfo);
    }


    @Override
    public ListProjectMVP.ListProjectInfo getListProjectInfo() {
        //try {
        return new ListProjectMVP.ListProjectInfo(project.getText().toString());
    }

    private void onReturnClick() {
        Intent intent = new Intent(this, StartingScreenActivity.class);
        startActivity(intent);
    }

    @Override
    public void viewRecicleView() {
        Intent intent = new Intent(this, InfProjectActivity.class);
        startActivity(intent);

    }

    @Override
    public void openLocationActivity() {

    }


}