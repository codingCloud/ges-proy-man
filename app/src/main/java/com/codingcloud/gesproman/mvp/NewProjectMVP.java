package com.codingcloud.gesproman.mvp;

import android.app.Activity;

public interface NewProjectMVP {
    interface Model{

    }
    interface Presenter{

        void nextClick();
    }
    interface View{

        void onNextClick();

        void onCloseClick();

        void onClearClick();

        Activity getActivity();

        NewProjectInfo getNewProjectInfo();

        void showGeneralError();
    }
    class NewProjectInfo {

        private String nameProject;
        private String budGeneral;
        private String cant;
        private String spTimeFrame;
        private String nameOwner;
        private String nameAuditor;
        private String nameExecutor;

        public NewProjectInfo(String nameProject, String budGeneral, String nameOwner, String nameAuditor, String nameExecutor) {
            this.nameProject = nameProject;
            this.budGeneral = budGeneral;
            //this.cant = cant;
            //this.spTimeFrame = spTimeFrame;
            this.nameOwner = nameOwner;
            this.nameAuditor = nameAuditor;
            this.nameExecutor = nameExecutor;
        }

        public String getNameProject() {
            return nameProject;
        }
        public String getBudGeneral() {
            return budGeneral;
        }
        /*public String getCant() {
            return cant;
        }
        public String getSpTimeFrame() {
            return spTimeFrame;
        }
         */
        public String getNameOwner() {
            return nameOwner;
        }
        public String getNameAuditor() {
            return nameAuditor;
        }
        public String getNameExecutor() {
            return nameExecutor;
        }
    }
}
