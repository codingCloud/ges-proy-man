package com.codingcloud.gesproman.mvp;

import android.app.Activity;
import android.widget.Spinner;

import com.codingcloud.gesproman.model.database.entities.User;

public interface RegisterMVP {

    interface Model{
        void createUser(UserInfo user, CreateUserCallback callback);

        interface CreateUserCallback{
            void onSuccess();
            void onFailure(String error);
        }
    }

    interface Presenter{
        boolean isPasswordValid(String password);

        void saveClick();
    }

    interface View{

        Activity getActivity();

        void showGeneralError();

        void showPasswordError();

        void validatePassword();

        UserInfo getUserInfo();

        void onClearClick();

        void onCreateClick();

        void showGeneralMessage(String error);
    }

    class UserInfo{
        private String name;
        private String identification;
        private String profession;
        private String email;
        private String phone;
        private String password;
        private String type;

        public UserInfo(String name, String identification, String profession, String email, String password, String phone, String type) {
            this.name = name;
            this.identification = identification;
            this.profession = profession;
            this.email = email;
            this.phone = phone;
            this.password = password;
            this.type=type;

        }

        public String getType() { return type; }

        public String getName() { return name; }

        public String getIdentification() { return identification; }

        public String getProfession() { return profession; }

        public String getEmail() { return email; }

        public String getPhone() { return phone; }

        public String getPassword() { return password; }

        @Override
        public String toString() {
            return "UserInfo{" +
                    "name='" + name + '\'' +
                    ", identification='" + identification + '\'' +
                    ", profession='" + profession + '\'' +
                    ", email='" + email + '\'' +
                    ", phone='" + phone + '\'' +
                    ", password='" + password + '\'' +
                    ", type='" + type + '\'' +
                    '}';
        }
    }
}
