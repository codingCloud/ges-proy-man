package com.codingcloud.gesproman.mvp;

public interface ProjectProgressMVP {
    interface Model {

    }

    interface Presenter {

        void resourcesProgress();

        void timeProgress();

        void qualityProgress();


    }

    interface View {

        void openResourceProgress();

        void openTimeProgress();

        void openQualityProgress();
    }
}
