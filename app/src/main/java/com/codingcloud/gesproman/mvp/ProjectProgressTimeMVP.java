package com.codingcloud.gesproman.mvp;

public interface ProjectProgressTimeMVP {
    interface Model {

    }

    interface Presenter {
        void progressTime();
    }

    interface View {
        void saveProgressTime();
    }
}
