package com.codingcloud.gesproman.mvp;

import android.app.Activity;

public interface StartingScreenMVP {
    interface Model{

    }

    interface Presenter{
        void signOff();
    }

    interface View{
        Activity getActivity();
        void onCloseClick();

    }
}
