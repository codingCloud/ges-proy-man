package com.codingcloud.gesproman.mvp;

import android.app.Activity;

public class ProjectProgressResourcesMVP {

    public interface Model{

    }
    public interface Presenter{

        boolean saveClick(); //Guardar Actividad

    }
    public interface View{

        ProjectProgressResourcesInfo getProjectProgressResourcesInfo();

        void onSaveClick();

        void onCancelClick();

        void onCloseClick();

        Activity getActivity();

        void showGeneralError();
    }

    public static class ProjectProgressResourcesInfo {
        private String activity;
        private String budget;
        private String realBudget;

        //Constructor
        public ProjectProgressResourcesInfo (String activity, String budget, String realBudget) {
            this.activity = activity;
            this.budget = budget;
            this.realBudget = realBudget;
        }
        //Getters
        public String getActivity() {
            return activity;
        }
        public String getBudget() {
            return budget;
        }
        public String getRealBudget() {
            return realBudget;
        }
    }

}
