package com.codingcloud.gesproman.mvp;

import android.app.Activity;

public interface LoginMVP {
    interface Model{
        void validateCredentials(String email, String password, ValidateCredentialsCallback callback);
        boolean isAuthenticated();
        interface ValidateCredentialsCallback{
            void onSuccess();
            void onFailure(String error);
        }
    }

    interface Presenter{
        void login();
        void isAuthenticated();
       // void isLogged();

    }

    interface View{
        Activity getActivity();
        LoginInfo getLoginInfo();

        void showEmailError(String error);
        void showPasswordError(String error);
        void showGeneralMessage(String error);

        void openNewActivity();
        void openRegisterActivity();

        void startWaiting();
        void stopWaiting();
    }

    class LoginInfo{
        private String email;
        private String password;

        public LoginInfo(String email,String password){
            this.email = email;
            this.password = password;
        }

        public void setEmail(String email) { this.email = email; }

        public void setPassword(String password) { this.password = password; }

        public String getEmail() {
            return email;
        }

        public String getPassword() {
            return password;
        }
    }
}
