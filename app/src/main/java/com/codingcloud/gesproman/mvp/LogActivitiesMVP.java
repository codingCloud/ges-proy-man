package com.codingcloud.gesproman.mvp;

import android.app.Activity;

import java.util.List;

public interface LogActivitiesMVP {
    interface Model {

        void loadLogActivities(LogActivitiesMVP.Model.LoadLogActivitiesCallback callback);

        interface LoadLogActivitiesCallback {
            void showLogActivitiesInfo(List<LogActivitiesParametersInfo> logActivitiesParametersInfo);
        }
    }

    interface Presenter {
        void saveClick(); //Guardar Actividad

        boolean addClick(); //Añadir Actividad

        void deleteClick(); //Eliminar Actividad

        boolean addParametersClick();

        void queryData();

        void onItemSelected(LogActivitiesMVP.LogActivitiesInfo logActivitiesInfo);

        void onItemSelectedParameters(LogActivitiesMVP.LogActivitiesParametersInfo logActivitiesParametersInfo);

        }

    interface View {

        Activity getActivity();

        LogActivitiesInfo getLogActivitiesInfo();

        LogActivitiesParametersInfo getLogActivitiesParametersInfo();

        void showLogActivitiesInfo(List<LogActivitiesMVP.LogActivitiesParametersInfo> logActivitiesParametersInfo);

        boolean onSaveClick(); //Guardar actividad

        void onAddClick(); //Añadir nueva actividad

        void onDeleteClick(); //Eliminar actividad

        void showGeneralError(); //Falta completar campos

        void showParametersError(); //Falta completar parámetro

        void showActivityError(); //Es la primera actividad

        void onClearClick(); //Limpiar campos

        void openLocationActivity();

        void onAddParametersClick();
    }

    class LogActivitiesInfo {
        private String nameActivity;
        private String budget;
        private String QualityParameters1;

        //Constructor
        public LogActivitiesInfo(String nameActivity, String budget, String qualityParameters1) {
            this.nameActivity = nameActivity;
            this.budget = budget;
            QualityParameters1 = qualityParameters1;
        }
        //Getters
        public String getNameActivity() {
            return nameActivity;
        }
        public String getBudget() {
            return budget;
        }
        public String getQualityParameters1() {
            return QualityParameters1;
        }
    }

    class LogActivitiesParametersInfo {
        private String parameters;

        //Constructor
        public LogActivitiesParametersInfo(String parameters) {
            this.parameters = parameters;
        }

        //Get
        public String getParameters() {
            return parameters;
        }
    }
}
