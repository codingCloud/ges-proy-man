package com.codingcloud.gesproman.mvp;

import android.app.Activity;

public interface ProjectProgressQualyMVP {

    interface Model {

    }

    interface Presenter {

        void saveClick();
    }

    interface View {
        Object getActivity();

        void onClearClick();

        void onCloseClick();

        boolean onSaveClick();

        void showGeneralError();

        ProjectProgressQualyInfo getProjectProgressQualyInfo();


    }

    class ProjectProgressQualyInfo {
        
        private String quality1;
        private String quality2;
        private String note1;
        private String note2;

        public ProjectProgressQualyInfo(String quality1, String quality2, String note1, String note2) {
            this.quality1 = quality1;
            this.quality2 = quality2;
            this.note1 = note1;
            this.note2 = note2;
        }

        //Getters

        public String getQuality1() { return quality1; }

        public String getQuality2() { return quality2; }

        public String getNote1() { return note1; }

        public String getNote2() { return note2; }
    }
    
    
}
