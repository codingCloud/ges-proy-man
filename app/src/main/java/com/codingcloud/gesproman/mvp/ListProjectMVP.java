package com.codingcloud.gesproman.mvp;

import android.app.Activity;

import java.util.List;

public interface ListProjectMVP {

    interface Model{
        void loadListProject(ListProjectMVP.Model.LoadListProjectCallback callback);

        interface LoadListProjectCallback {
            void showListProjectInfo(List<ListProjectMVP.ListProjectInfo> listProjectInfo);
        }
    }
    interface Presenter{

        void queryData();

        void onItemSelected(ListProjectInfo listProjectInfo);

    }
    interface View{

        Activity getActivity();

        ListProjectInfo getListProjectInfo();

        void showListProjectInfo(List<ListProjectMVP.ListProjectInfo> listProjectInfo);

        void viewRecicleView();

        void openLocationActivity();


    }

    public class ListProjectInfo {
        private String project;

        //Constructor
        public ListProjectInfo(String project) {
            this.project = project;
        }
        //Getters
        public String getProject() {
            return project;
        }
    }
}
