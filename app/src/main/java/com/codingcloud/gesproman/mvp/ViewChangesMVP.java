package com.codingcloud.gesproman.mvp;

import android.app.Activity;

import java.util.List;

public interface ViewChangesMVP {

    interface Model{
        void loadViewChanges(LoadViewChangesCallback callback);

        interface LoadViewChangesCallback {
            void showViewChangesInfo(List<ViewChangesInfo> viewChangeInfo);
        }


    }

    interface Presenter{

        void queryData(); //TODO: Hacer el proceso que se hizo en Payments

        void onItemSelected(ViewChangesMVP.ViewChangesInfo viewChangesInfo);

        //void enableApproveClick();

        //void enableDisapproveClick();

    }

    interface View{

        Activity getActivity();

        void showViewChangesInfo(List<ViewChangesInfo> viewChangesInfo);

        ViewChangesInfo getViewChangesInfo();

        void approveDisapprove();

        void onApproveClick();

        void onDisapproveClick();

        void onReturnClick();

        void onCloseClick();

        void showNoChanges(); //No hay más cambios pendientes

        //void disableApprove();

        //void enableApprove();

        //void disableDisapprove();

        //void enableDisapprove();

        void openLocationActivity();

    }

    class ViewChangesInfo {
        private String infRequest;
        private String infDescription;


        //Constructor
        public ViewChangesInfo(String infRequest, String infDescription) {
            this.infRequest = infRequest;
            this.infDescription = infDescription;
        }

        //Getters
        public String getInfRequest() {
            return infRequest;
        }
        public String getInfDescription() {
            return infDescription;
        }
    }
}
