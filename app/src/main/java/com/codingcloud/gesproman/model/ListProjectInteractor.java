package com.codingcloud.gesproman.model;

import android.app.Activity;

import com.codingcloud.gesproman.mvp.ListProjectMVP;

import java.util.Arrays;
import java.util.List;

public class ListProjectInteractor implements ListProjectMVP.Model {

    //Simular los datos de la base de datos
    private List<ListProjectMVP.ListProjectInfo> data;

    //Constructor

    public ListProjectInteractor(Activity activity) {
        data = Arrays.asList(
                new ListProjectMVP.ListProjectInfo("Proyecto 1"),
                new ListProjectMVP.ListProjectInfo("Proyecto 2"),
                new ListProjectMVP.ListProjectInfo("Proyecto 3")
        );
    }

    @Override
    public void loadListProject(LoadListProjectCallback callback) {
        callback.showListProjectInfo(this.data);
    }
}
