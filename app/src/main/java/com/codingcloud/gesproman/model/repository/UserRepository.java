package com.codingcloud.gesproman.model.repository;

import android.content.Context;
import android.util.Log;

/*import androidx.annotation.NonNull;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
*/
import com.codingcloud.gesproman.model.database.UserDatabase;
import com.codingcloud.gesproman.model.database.dao.UserDao;
import com.codingcloud.gesproman.model.database.entities.User;
import com.codingcloud.gesproman.mvp.RegisterMVP;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class UserRepository {

    private static UserRepository instance;

    public static UserRepository getInstance(Context context){
        if (instance==null){
            instance = new UserRepository(context);
        }
        return instance;
    }

    private final static Boolean USE_DATABASE = Boolean.FALSE;

    private final UserDao userDao;

    // Write a message to the database
    private final DatabaseReference userRef;


    private UserRepository(Context context) {
        userDao = UserDatabase.getDatabase(context).getUserDao();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        userRef = database.getReference("user");

        loadInitData();
    }

    /*private void loadInitData() {
        userDao.insert(
                new User(73207881,"Roger Carrascal","carrascalfuentes@gmail.com","J123456r","Ingeniero","Master"),
                new User(12345678,"Prueba","prueba@prueba.com","P123456a","Prueba","Propietario")

        );

    }*/
    private void loadInitData() {
        if (USE_DATABASE) {
            userDao.insert(
                    new User(73207881,"Roger Carrascal", "carrascalfuentes@gmail.com", "J123456r", "Ingeniero", "Master"),
                    new User(12345678,"Prueba", "prueba@prueba.com", "P123456a", "Admin", "Propietario")

            );
        }else {
            String username = "prueba@prueba.com".replace('@', '_').replace('.', '_');
            userRef.child(username).child("identification").setValue(12345678);
            userRef.child(username).child("name").setValue("Prueba");
            userRef.child(username).child("email").setValue("prueba@prueba.com");
            userRef.child(username).child("password").setValue("P123456a");
            userRef.child(username).child("profession").setValue("Admin");
            userRef.child(username).child("type").setValue("Propietario");

            username = "carrascalfuentes@gmail.com".replace('@', '_').replace('.', '_');
            userRef.child(username)
                    .setValue(new User(73207881,"Roger Carrascal", "carrascalfuentes@gmail.com", "J123456r", "Ingeniero", "Master"));

        }
    }

    public void save(RegisterMVP.UserInfo user){
        System.out.println("aquí si");
        String introUser= user.getEmail().replace('@', '_').replace('.', '_');
        userRef.child(introUser).setValue(user);
    }

    public void getUserByEmail(String email, UserCallback callback) {
        if (USE_DATABASE){
            callback.onSuccess(userDao.getUserByEmail(email));
        }else {
            // Usar firebase
            String username = email.replace('@', '_').replace('.', '_');
            userRef.child(username).get()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()){
                            User value = task.getResult().getValue(User.class);
                            Log.d(UserRepository.class.getSimpleName(), "Value is: " + value);
                            if (value !=null) {
                                callback.onSuccess(value);
                            }else {
                                callback.onFailure();
                            }
                        }else {
                            callback.onFailure();
                            //Log.w(UserRepository.class.getSimpleName(), "Failed to read value.", error.toException());
                        }
                    });
        }
    }

    public void getAll(UserCallback<List<User>> callback){
        userRef.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()){
                DataSnapshot dataSnapshot= task.getResult();
                if (dataSnapshot.hasChildren()){
                    List<User> users = new ArrayList<>();
                    for (DataSnapshot snapshot: dataSnapshot.getChildren()) {
                        snapshot.child("name").getValue(String.class);
                        User user = snapshot.getValue(User.class);
                        Log.d(UserRepository.class.getSimpleName(),user.toString());
                        users.add(user);
                    }
                    callback.onSuccess(users);
                }else {
                    callback.onSuccess(new ArrayList<>());
                }
            }else {
                callback.onFailure();

            }
        });
    }

    public interface UserCallback<T>{
        void onSuccess(T data);
        void onFailure();
    }

}
