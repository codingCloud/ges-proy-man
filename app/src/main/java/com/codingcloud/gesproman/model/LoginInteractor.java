package com.codingcloud.gesproman.model;

import android.content.Context;
//import android.util.Log;

//import com.codingcloud.gesproman.model.database.entities.User;
import com.codingcloud.gesproman.model.repository.FirebaseAuthRepository;
import com.codingcloud.gesproman.model.repository.UserRepository;
import com.codingcloud.gesproman.mvp.LoginMVP;

//import java.util.HashMap;
import java.util.List;
//import java.util.Map;

public class LoginInteractor implements LoginMVP.Model {

    private UserRepository userRepository;
    private FirebaseAuthRepository firebaseAuthRepository;

    public LoginInteractor(Context context){
        userRepository = UserRepository.getInstance(context);
        firebaseAuthRepository = FirebaseAuthRepository.getInstance(context);
    }

    @Override
    public void validateCredentials(String email, String password, ValidateCredentialsCallback callback) {
        firebaseAuthRepository.authenticate(email, password,
                new FirebaseAuthRepository.FirebaseAuthCallback(){
                    @Override
                    public void onSuccess() {
                        callback.onSuccess();
                    }
                    @Override
                    public void onFailure() {
                        callback.onFailure("Credenciales inválidas");
                    }
                });
        /*userRepository.getUserByEmail(email, new UserRepository.UserCallback<User>() {
            @Override
            public void onSuccess(User user) {
                if (user==null){
                    callback.onFailure("Correo electronico no existe");
                }else if (!user.getPassword().equals(password)){
                    callback.onFailure("Contraseña incorrecta");
                }else {
                    callback.onSuccess();
                }
            }

            @Override
            public void onFailure() {
                callback.onFailure("Error accediento a los datos");
            }
        });

        userRepository.getAll(new UserRepository.UserCallback<List<User>>() {
            @Override
            public void onSuccess(List<User> data) {
                for (User user: data){
                    Log.d(LoginInteractor.class.getSimpleName(),user.toString());
                }

            }

            @Override
            public void onFailure() {
                Log.w(LoginInteractor.class.getSimpleName(),"Problemas para obetener datos");
            }
        });*/
    }
    public boolean isAuthenticated() {
        return firebaseAuthRepository.isAuthenticated();
    }

    /*private Map<String,String> users;

    public LoginInteractor(){
        users=new HashMap<>();
        users.put("carrascalfuentes@gmail.com","J123456r");
        users.put("prueba@prueba.com","P123456a");
    }

    @Override
    public void validateCredentials(String email, String password, ValidateCredentialsCallback callback) {
        try {
            Thread.sleep(5000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }

        if (users.get(email)==null){
            callback.onFailure("Correo electronico no existe");
        }else if (!users.get(email).equals(password)){
            callback.onFailure("Contraseña incorrecta");
        }else {
            callback.onSuccess();
        }
    }*/

}
