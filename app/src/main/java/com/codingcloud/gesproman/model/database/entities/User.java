package com.codingcloud.gesproman.model.database.entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class User implements Serializable {

    @PrimaryKey
    private int identification;

    private String name;

    private String email;
    private String profession;
    private String password;
    private String type;
    private Boolean enable;

    public User(int identification, String name, String email, String password, String profession, String type) {
        this.identification=identification;
        this.name = name;
        this.email = email;
        this.password = password;
        this.profession = profession;
        this.type = type;
        this.enable = true;
    }

    public User() {
    }

    public String getProfession() { return profession; }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public int getIdentification() {
        return identification;
    }

    public void setIdentification(int identification) {
        this.identification = identification;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    @NonNull
    @Override
    public String toString() {
        return "User{" +
                "identification='" + identification + '\'' +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", profession='" + profession + '\'' +
                ", type='" + type + '\'' +
                ", enable=" + enable +
                '}';
    }
}
