package com.codingcloud.gesproman.model.repository;

import android.content.Context;

import com.codingcloud.gesproman.model.database.entities.User;
import com.codingcloud.gesproman.mvp.RegisterMVP;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class FirebaseAuthRepository {

    private static FirebaseAuthRepository instance;

    public static FirebaseAuthRepository getInstance(Context context) {
        if (instance == null) {
            instance = new FirebaseAuthRepository(context);
        }
        return instance;
    }

    private UserRepository userRepository;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser currentUser;

    private FirebaseAuthRepository(Context context) {
        this.userRepository = UserRepository.getInstance(context);
        this.firebaseAuth = FirebaseAuth.getInstance();
    }

    public boolean isAuthenticated() {
        return getCurrentUser() != null;
    }

    public FirebaseUser getCurrentUser() {
        if (currentUser == null) {
            currentUser = firebaseAuth.getCurrentUser();
        }
        return currentUser;
    }

    public void registerNewUser(RegisterMVP.UserInfo user, FirebaseAuthCallback callback) {
        firebaseAuth.createUserWithEmailAndPassword(user.getEmail(), user.getPassword())
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        currentUser = firebaseAuth.getCurrentUser();
                        userRepository.save(user);
                        callback.onSuccess();
                    } else {
                        callback.onFailure();

                    }
                });
    }

    public void authenticate(String email, String password, FirebaseAuthCallback callback) {
        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        currentUser = firebaseAuth.getCurrentUser();
                        callback.onSuccess();
                    } else {
                        callback.onFailure();
                    }
                });
    }

    public void logout(FirebaseAuthCallback callback) {
        if (currentUser != null) {
            firebaseAuth.signOut();
            currentUser = null;
            callback.onSuccess();
        } else {
            callback.onFailure();
        }
    }

    public void signInWithCredentials(AuthCredential credential, FirebaseAuthCallback callback){
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        currentUser = firebaseAuth.getCurrentUser();
                        callback.onSuccess();
                    } else {
                        callback.onFailure();
                    }
                });
    }

    public interface FirebaseAuthCallback {
        void onSuccess();

        void onFailure();
    }

}
