package com.codingcloud.gesproman.model.database;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.codingcloud.gesproman.model.database.dao.UserDao;
import com.codingcloud.gesproman.model.database.entities.User;

@Database(entities = {User.class},version = 1)
public abstract class UserDatabase extends RoomDatabase {

    public abstract UserDao getUserDao();

    private static volatile UserDatabase INSTANCE;

    public static UserDatabase getDatabase(Context context) {

        if (INSTANCE == null) {
            INSTANCE = Room
                    .databaseBuilder(context.getApplicationContext(), UserDatabase.class, "database-name")
                    .allowMainThreadQueries()
                    .build();
        }
        return INSTANCE;
    }


}
