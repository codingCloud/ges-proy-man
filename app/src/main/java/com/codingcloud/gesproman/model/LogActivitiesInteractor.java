package com.codingcloud.gesproman.model;


import android.app.Activity;

import com.codingcloud.gesproman.mvp.ListProjectMVP;
import com.codingcloud.gesproman.mvp.LogActivitiesMVP;
import com.codingcloud.gesproman.view.LogActivities;

import java.util.Arrays;
import java.util.List;

public class LogActivitiesInteractor implements LogActivitiesMVP.Model {

    //Simular los datos de la base de datos
    public List<LogActivitiesMVP.LogActivitiesParametersInfo> data;

    //Constructor

    public LogActivitiesInteractor(Activity activity) {
        data = Arrays.asList(
                new LogActivitiesMVP.LogActivitiesParametersInfo("Que esté nivelado"),
                new LogActivitiesMVP.LogActivitiesParametersInfo("Que sea estético")
        );
    }

    @Override
    public void loadLogActivities(LoadLogActivitiesCallback callback) {
        callback.showLogActivitiesInfo(this.data);
    }
}
