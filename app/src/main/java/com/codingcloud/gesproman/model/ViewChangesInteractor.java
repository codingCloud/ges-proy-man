package com.codingcloud.gesproman.model;


import android.app.Activity;

import com.codingcloud.gesproman.mvp.ViewChangesMVP;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ViewChangesInteractor implements ViewChangesMVP.Model {

    //Simular los datos de la base de datos
    private List<ViewChangesMVP.ViewChangesInfo> data;

    //Constructor

    public ViewChangesInteractor(Activity activity) {
        data = Arrays.asList(
                new ViewChangesMVP.ViewChangesInfo("Aumentar 2 días al cronograma", "No ha llegado el material"),
                new ViewChangesMVP.ViewChangesInfo("Aumentar presupuesto", "Por alza de precios")
        );
    }

    @Override
    public void loadViewChanges(LoadViewChangesCallback callback) {
        callback.showViewChangesInfo(this.data);
    }
}

