package com.codingcloud.gesproman.model;

import android.content.Context;
import android.util.Log;

import com.codingcloud.gesproman.model.database.entities.User;
import com.codingcloud.gesproman.model.repository.FirebaseAuthRepository;
import com.codingcloud.gesproman.model.repository.UserRepository;
import com.codingcloud.gesproman.mvp.RegisterMVP;

public class RegisterInteractor implements RegisterMVP.Model {

    private UserRepository userRepository;
    private FirebaseAuthRepository firebaseAuthRepository;

    public RegisterInteractor(Context context) {
        userRepository = UserRepository.getInstance(context);
        firebaseAuthRepository = FirebaseAuthRepository.getInstance(context);
    }

    @Override
    public void createUser(RegisterMVP.UserInfo user, CreateUserCallback callback) {
        firebaseAuthRepository.registerNewUser(user,
                new FirebaseAuthRepository.FirebaseAuthCallback(){
                    @Override
                    public void onSuccess() {

                        Log.d(UserRepository.class.getSimpleName(), "Value is: " + user.toString());

                        callback.onSuccess();
                    }
                    @Override
                    public void onFailure() {
                        callback.onFailure("Ay Carambas");
                    }
                });
    }
}
